# gnome-shell-extension-taskbar-2020

TaskBar 2020 displays icons of running applications on the top panel or alternatively on a new bottom panel.

https://github.com/c0ldplasma/gnome-shell-extension-taskbar

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-taskbar-2020.git
```

